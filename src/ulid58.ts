import { base58 } from './baseX.js'
import { getRandomValues } from './crypto.js'

/**
 * Simple ULID (https://github.com/ulid/spec) implementation base58 encoded 
 * with a 7 digits 0.01s epoch time part (~41bit) and 14 digits random (~82bit).
 */
export function createId(seedTime = Date.now()) {
	return encodeTime(limitTime(seedTime)) + getRandom()
}

export function monotonicFactory() {
	let time = 0
	let random: string
	return function createId(seedTime = Date.now()) {
		seedTime = limitTime(seedTime)
		if (seedTime <= time) {
			random = base58.increment(random)
		} else {
			random = getRandom()
			time = seedTime
		}
		return encodeTime(time) + random
	}
}

function getRandom() {
	return base58.encode(getRandomValues(11)).substring(1, 15)
}

export const epochTime2020 = 1577836800
export const epochTime2020Millis = epochTime2020 * 1000

const maxTime = Math.pow(58, 7) - 1
function encodeTime(time: number) {
	if (time < 0 || time > maxTime)
		throw new Error(`Invalid time value ${time}!`)
	return base58.encode(time).padStart(7, '0')
}

/**
 * Limit time range for 7 base58 digits.
 *
 * 62^7 / 365 / 24 / 3600 / 100 ≈ 700 years
 *
 * With the random part of a little less than 84bits and the time part of 38bits
 * this ID could be converted into a standard UUID v4 with 122bit for the next
 * 80 years (~ 2^38 / 365 / 24 / 3600 / 100 ≈ 87 years).
 *
 * @param time Time in milliseconds since 1970
 * @returns Time in 1/100 seconds since 2020
 */
function limitTime(time: number): number {
	// 1/100 second should be enough
	return Math.floor((time - epochTime2020Millis) / 10)
}

