import { describe, expect, test } from 'vitest'
// import { base32 } from '../baseX.js'
import * as base32 from '../base32.js'

describe('utilities for base32 encoding', () => {
	const testDataNumber = [
		// cspell:disable
		[0, '0'], [31, 'z'], [32, '10'], [33, '11'],
		[41, '19'], [42, '1a'], [43, '1b'], [63, '1z'], [64, '20'],
		[2147483647, '1zzzzzz'],
		[Number.MAX_SAFE_INTEGER - 1, '7zzzzzzzzzy'],
		[Number.MAX_SAFE_INTEGER, '7zzzzzzzzzz'],
		// cspell:enable
	]

	test('encode Base32', () => {
		for (const [num, str] of testDataNumber)
			expect(base32.encode(num as number)).toEqual(str)
	})

	test('decode Base32', () => {
		for (const [num, str] of testDataNumber)
			expect(base32.decodeNumber(str as string)).toEqual(num)
	})

	const testDataBytes = [
		// cspell:disable
		[[0x00], '00'],
		[[0x01], '01'], [[0x02], '02'], [[0x0f], '0f'], [[0x10], '0g'],
		[[0xff], '7z'],
		[[0x01, 0x00], '0080'], [[0x01, 0x01], '0081'], [[0xff, 0xff], '1zzz'],
		[[0x00, 0x01], '0001'], [[0x01, 0xff], '00fz'], [[0x00, 0xff], '007z'],
		[[0x74, 0x65, 0x73, 0x74], '1u6awvm'],
		[[0x74, 0x65, 0x73, 0x74, 0x01], 'ehjq6x01'],
		[[0x00, 0x11, 0x22, 0x33, 0x44, 0x55], '0024h36h2n'],
		[
			[0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33, 0x14, 0x18, 0xb6,
				0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4],
			'2w2qd15ym7wk650rprtuhwvk26eqcqym',
		],
		// cspell:enable
	]

	test('encode buffer as Base32', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base32.encode(new Uint8Array(bytes as number[]).buffer))
				.toEqual(str)
	})

	test('decode buffer as Base32', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base32.decode(str as string))
				.toEqual(new Uint8Array(bytes as number[]))
	})

	const testDataBytes2 = [
		// cspell:disable
		[[0x74, 0x65, 0x73, 0x74], '1T6AWVM'],
		[[0x00, 0x11, 0x22, 0x33, 0x44, 0x55], '0024H36H2N'],
		[
			[0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33, 0x14, 0x18, 0xb6,
				0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4],
			'2W2QD15YM7WK650RPRSTHWVK26EQCQYM',
		],
		// cspell:enable
	]

	test('encode buffer as Base32 Crockford', () => {
		for (const [bytes, str] of testDataBytes2)
			expect(base32.encode(new Uint8Array(bytes as number[]),
				base32.alphabet.crockford)).toEqual(str)
	})

	test('decode buffer as Base32 Crockford', () => {
		for (const [bytes, str] of testDataBytes2)
			expect(base32.decode(str as string,
				base32.alphabet.crockford)).toEqual(new Uint8Array(bytes as number[]))
	})

	test('increment Base32', () => {
		expect(base32.increment('0')).toEqual('1')
		expect(base32.increment('9')).toEqual('a')
		expect(base32.increment('a')).toEqual('b')
		expect(base32.increment('0z')).toEqual('10')
		expect(base32.increment('0zz')).toEqual('100')
		expect(base32.increment('0zzz')).toEqual('1000')
		expect(() => base32.increment('z')).toThrow()
		expect(() => base32.increment('zz')).toThrow()
		expect(() => base32.increment('zzz')).toThrow()
	})
})
