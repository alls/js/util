import { describe, expect, test } from 'vitest'
import * as error from '../error.js'

describe('utilities for errors', () => {
	test('toJson', () => {
		expect(error.toJson('')).toEqual(null)
		expect(error.toJson({ a: 1 } as unknown as Error)).toEqual({ a: 1 })
		expect(error.toJson(new Error()))
			.toEqual({ stack: expect.arrayContaining(['Error:']) as unknown })
		const stack = expect.arrayContaining(['Error: a']) as unknown
		expect(error.toJson(new Error('a'))).toEqual({ message: 'a', stack })
		const err: Error & { b?: number } = new Error('a')
		err.b = 2
		expect(error.toJson(err)).toEqual({ message: 'a', b: 2, stack })
	})

	test('toJson subclass', () => {
		class TestError extends Error {
			data = 3
		}
		const err: Error & { b?: number } = new TestError('a')
		err.b = 2
		const stack = expect.arrayContaining(['Error: a']) as unknown
		expect(error.toJson(err)).toEqual({ message: 'a', b: 2, data: 3, stack })
	})
})
