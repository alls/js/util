import { describe, expect, test } from 'vitest'
import * as str from '../str.js'

describe('Str', () => {
	test('substringBetween', () => {
		expect(str.substringBetween('abc', 'a', 'c')).toEqual('b')
		expect(str.substringBetween('abc', 'a', 'b')).toEqual('')
		expect(str.substringBetween('abc', 'a', 'a')).toEqual('bc')
		expect(str.substringBetween('abc', 'c', '')).toEqual('')
		expect(str.substringBetween('abc', '', '')).toEqual('abc')
		expect(str.substringBetween('abc', 'a', 'd')).toEqual('bc')
		expect(str.substringBetween('abc', 'd', 'c')).toEqual('ab')
	})

	test('toCamelCase', () => {
		expect(str.toCamelCase('abc')).toEqual('abc')
		expect(str.toCamelCase('ab c')).toEqual('abC')
		expect(str.toCamelCase('ab cd')).toEqual('abCd')
		expect(str.toCamelCase('ab-c')).toEqual('abC')
		expect(str.toCamelCase('aBc')).toEqual('aBc')
		expect(str.toCamelCase('Ab cd')).toEqual('abCd')
		expect(str.toCamelCase('ab Cd')).toEqual('abCd')
		expect(str.toCamelCase('ab cd ')).toEqual('abCd')
		expect(str.toCamelCase(' ab cd')).toEqual('abCd')
		expect(str.toCamelCase(' Ab cd')).toEqual('abCd')
		expect(str.toCamelCase('a1b c2d')).toEqual('a1bC2d')
		expect(str.toCamelCase('1b 2d')).toEqual('1b2d')
		expect(str.toCamelCase('123')).toEqual('123')
	})
})
