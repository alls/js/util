import { describe, expect, test } from 'vitest'
import * as date from '../date.js'

describe('utilities for dates', () => {
	test('isoDate', () => {
		for (const s of [
			'2020-02-02', '2020-02-02T00:00:00', '2020-02-02T00:00:01',
			'2020-02-02T23:59:59.999',
		])
			expect(date.toIsoDateString(new Date(s))).toBe('2020-02-02')
		expect(date.parseIsoDate('2020-02-02')?.toISOString())
			.toBe(new Date(2020, 1, 2, 0, 0, 0).toISOString())
	})

	test('isoTime', () => {
		const tz = date.toIsoTzString(new Date('2020-02-02'))
		for (const [d, r] of [
			['2020-02-02T23:59:59', '23:59:59' + tz],
			['2020-02-02T00:00:00', '00:00:00' + tz],
			['2020-02-02T00:00:01', '00:00:01' + tz],
			['2020-02-02T23:59:59.999', '23:59:59.999' + tz],
		])
			expect(date.toIsoTimeString(new Date(d))).toBe(r)
		const t = date.parseIsoTime('23:59:59')
		expect(t).not.toBeNull()
		if (t)
			expect(t.toISOString())
				.toBe(new Date(t.getFullYear(), t.getMonth(), t.getDate(), 23, 59, 59)
					.toISOString())
	})

	test('isoDateTime', () => {
		const tz = date.toIsoTzString(new Date('2020-02-02'))
		for (const [d, r] of [
			['2020-02-02T23:59:59', '2020-02-02T23:59:59' + tz],
			['2020-02-02T00:00:00', '2020-02-02T00:00:00' + tz],
			['2020-02-02T00:00:01', '2020-02-02T00:00:01' + tz],
			['2020-02-02T23:59:59.999', '2020-02-02T23:59:59.999' + tz],
		])
			expect(date.toIsoDateTimeString(new Date(d))).toBe(r)
		for (const s of [
			'2020-02-02T00:00:00Z', '2020-02-02T00:00:01+01:00',
			'2020-02-02T23:59:59.999-01:00',
		])
			expect(date.parseIsoDateTime(s)?.toISOString())
				.toBe(new Date(s).toISOString())
	})
})
