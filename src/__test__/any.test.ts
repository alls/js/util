import { describe, expect, test } from 'vitest'
import * as any from '../any.js'

describe('utilities for values of any type', () => {
	test('stringify', () => {
		const t = any.stringify
		expect(t({})).toBe('{}')
		expect(t({ a: 1 })).toBe('{a:1}')
		expect(t({ a: '1' })).toBe("{a:'1'}")
		expect(t({ a: true })).toBe('{a:true}')
		expect(t({ a: 1, b: 2 })).toBe('{a:1,b:2}')
		expect(t({ a: '1', b: '2' })).toBe("{a:'1',b:'2'}")
		expect(t({ a: 1, b: { c: 2 }, d: 3 })).toBe('{a:1,b:{c:2},d:3}')
		const now = new Date()
		expect(t({ a: now })).toBe(`{a:'${now.toISOString()}'}`)
		expect(t(new Error('test')))
			.toMatch(/^\{message:'test',.*stack:\[.+\]\}$/)
	})

	test('stringify with intent', () => {
		const t = any.stringify
		expect(t({}, ' ')).toBe('{}')
		expect(t({ a: 1 }, ' ')).toBe('{\n a: 1\n}')
		expect(t({ a: '1' }, ' ')).toBe("{\n a: '1'\n}")
		expect(t({ a: true }, ' ')).toBe('{\n a: true\n}')
		expect(t({ a: 1, b: 2 }, ' ')).toBe('{\n a: 1,\n b: 2\n}')
		expect(t({ a: '1', b: '2' }, ' ')).toBe("{\n a: '1',\n b: '2'\n}")
		expect(t({ a: 1, b: { c: 2 }, d: 3 }, ' ')).toBe(
			'{\n a: 1,\n b: {\n  c: 2\n },\n d: 3\n}')
		const now = new Date()
		expect(t({ a: now }, ' ')).toBe(`{\n a: '${now.toISOString()}'\n}`)
		expect(t(new Error('test'), ' '))
			.toMatch(/^\{\n message: 'test',\n stack: \[.+\]\n\}$/m)
	})
})
