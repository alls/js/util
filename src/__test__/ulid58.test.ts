import { describe, expect, test } from 'vitest'
import * as ulid58 from '../ulid58.js'

const fixTime = ulid58.epochTime2020Millis + 10

describe('ULID58', () => {
	test('generate', () => {
		expect(ulid58.createId()).toHaveLength(21)
		expect(ulid58.createId(fixTime)).toHaveLength(21)
	})

	test('minimal collision detection', () => {
		const ids: Record<string, number> = {}
		for (let i = 0; i < 1000; ++i) {
			const id = ulid58.createId(fixTime)
			expect(id in ids).toBe(false)
			ids[id] = 1
		}
	})

	test('generate monotonic', () => {
		const createId = ulid58.monotonicFactory()
		expect(createId()).toHaveLength(21)
		expect(createId(fixTime)).toHaveLength(21)
	})

	test('monotonic sortable', () => {
		const createId = ulid58.monotonicFactory()
		let lastId = createId()
		for (let i = 0; i < 1000; ++i) {
			const id = createId()
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('monotonic sortable fix time', () => {
		const createId = ulid58.monotonicFactory()
		let lastId = createId(fixTime)
		for (let i = 0; i < 1000; ++i) {
			const id = createId(fixTime)
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('extreme time', () => {
		const maxTime = ulid58.epochTime2020 * 1000 + 58 ** 7 * 10 - 1
		expect(ulid58.createId(maxTime)).toMatch('zzzzzzz')
		expect(() => ulid58.createId(maxTime + 1)).toThrow()
	})
})
