import { describe, expect, test } from 'vitest'
import * as obj from '../obj.js'

describe('utilities for objects', () => {
	test('filter properties', () => {
		expect(obj.filterProps({ a: 1, b: 2 }, v => v > 1)).toEqual({ b: 2 })
		expect(obj.filterProps({ a: 1, b: 2 }, v => v > 0)).toEqual({ a: 1, b: 2 })
		expect(obj.filterProps({ a: 1, b: 2 }, v => v > 2)).toEqual({})
	})

	test('filter properties subclass', () => {
		class A { a = 1 }
		class B extends A { b = 2 }
		expect(obj.filterProps(new B(), v => v > 1)).toEqual({ b: 2 })
		expect(obj.filterProps(new B(), v => v > 0)).toEqual({ a: 1, b: 2 })
		expect(obj.filterProps(new B(), v => v > 2)).toEqual({})
	})

	test('mapValues with another type', () => {
		expect(obj.mapValues({ a: 1 }, v => `${v}1`)).toEqual({ a: '11' })
	})

	test('mapValues', () => {
		expect(obj.mapValues({}, v => v)).toEqual({})
		expect(obj.mapValues({ a: 1 }, v => v + 1)).toEqual({ a: 2 })
		expect(obj.mapValues(new Error(), v => v))
			.toEqual({ stack: expect.stringMatching(/^Error/) as unknown })
		const stack = expect.stringMatching(/^Error: a/) as unknown
		expect(obj.mapValues(new Error('a'), v => v))
			.toEqual({ message: 'a', stack })
		const err: Error & { b?: number } = new Error('a')
		err.b = 2
		expect(obj.mapValues(err, v => v)).toEqual({ message: 'a', b: 2, stack })
	})

	test('mapValues subclass', () => {
		class A { a = 1 }
		class B extends A { b = 2 }
		expect(obj.mapValues(new B(), v => v + 1)).toEqual({ a: 2, b: 3 })
		class TestError extends Error {
			data = 3
		}
		const err: Error & { b?: number } = new TestError('a')
		err.b = 2
		expect(obj.mapValues(err, v => v))
			.toEqual({
				message: 'a', b: 2, data: 3,
				stack: expect.stringMatching(/^Error: a/) as unknown,
			})
	})

	test('delete properties', () => {
		expect(obj.deletePropsIn({ a: 1, b: 2 }, 'a')).toEqual({ b: 2 })
		expect(obj.deletePropsIn({ a: 1, b: 2 }, 'a', 'b')).toEqual({})
		expect(obj.deletePropsIn({ a: 1, b: 2 }, 'a', 'c' as 'b')).toEqual({ b: 2 })
	})

	test('delete equal properties', () => {
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 1 })).toEqual({ b: 2 })
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, {})).toEqual({ a: 1, b: 2 })
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 1, b: 2 })).toEqual({})
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 1, c: 2 }))
			.toEqual({ b: 2 })
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 0 }))
			.toEqual({ a: 1, b: 2 })
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 0, b: 1 }))
			.toEqual({ a: 1, b: 2 })
		expect(obj.deleteEqualPropsIn({ a: 1, b: 2 }, { a: 0, b: 2 }))
			.toEqual({ a: 1 })
	})

	test('deep equals true', () => {
		const e = (a: unknown, b: unknown) => {
			expect(obj.deepEquals(a, b)).toBe(true)
		}
		e({ a: 1, b: 2 }, { b: 2, a: 1 })
		e({ a: 1, b: { c: 3 } }, { b: { c: 3 }, a: 1 })
		e({ a: 1, b: [2] }, { b: [2], a: 1 })
		e([1, { a: 1, b: { c: 3 } }], [1, { b: { c: 3 }, a: 1 }])
	})

	test('deep equals false', () => {
		const e = (a: unknown, b: unknown) => {
			expect(obj.deepEquals(a, b)).toBe(false)
		}
		e({ a: 1, b: 2 }, { b: 2, a: 1, c: 3 })
		e({ a: 1, b: { c: 3 } }, { b: { c: 3, d: 4 }, a: 1 })
		e({ a: 1, b: [2] }, { b: [2, 3], a: 1 })
		e([1, { a: 1, b: { c: 3 } }], [{ b: { c: 3 }, a: 1 }, 1])
	})

	test('is empty', () => {
		expect(obj.isEmpty({})).toBe(true)
		expect(obj.isEmpty({ a: 1, b: 2 })).toBe(false)
		expect(obj.isEmpty({ a: () => { } })).toBe(false)
	})

	test('equal keys', () => {
		expect(obj.equalKeys({}, {})).toBe(true)
		expect(obj.equalKeys({ a: 1, b: 2 }, { a: 1, b: 2 })).toBe(true)
		expect(obj.equalKeys({ a: 1, b: 2 }, { a: 2, b: 3 })).toBe(true)
		expect(obj.equalKeys({ a: 1, b: 2 }, { a: 1, c: 2 })).toBe(false)
		expect(obj.equalKeys({ a: 1, b: 2 }, { a: 1, b: 2, c: 3 })).toBe(false)
	})

	test('exclude', () => {
		expect(obj.exclude({ a: 1, b: 2, c: 3 }, 'a')).toEqual({ b: 2, c: 3 })
		expect(obj.exclude({ a: 1, b: 2, c: 3 }, 'b')).toEqual({ a: 1, c: 3 })
		expect(obj.exclude({ a: 1, b: 2, c: 3 }, 'c')).toEqual({ a: 1, b: 2 })
		expect(obj.exclude({ a: 1, b: 2, c: 3 }, 'd' as 'a'))
			.toEqual({ a: 1, b: 2, c: 3 })
		expect(obj.exclude({ a: 1, b: 2, c: 3 }, 'a', 'c')).toEqual({ b: 2 })
		const obj2 = { a: 1, b: 2, c: 3 }
		const res = obj.exclude(obj2, 'b')
		expect(res).not.toBe(obj2)
		expect('b' in res).toBe(false)
	})

	test('swap keys with values', () => {
		expect(obj.swapKeysWithValuesIn({ a: 'b', b: 'c' }))
			.toEqual({ b: 'a', c: 'b' })
		expect(obj.swapKeysWithValuesIn({ a: 'b', b: 'b' }))
			.toEqual({ b: 'b' })
		expect(obj.swapKeysWithValuesIn({ a: 'b', b: 'a' }))
			.toEqual({ b: 'a', a: 'b' })
	})

	test('exclude null properties', () => {
		expect(obj.excludeNullProps({ a: 1, b: 2, c: null }))
			.toEqual({ a: 1, b: 2 })
		expect(obj.excludeNullProps({ a: 1, b: 2, c: undefined }))
			.toEqual({ a: 1, b: 2 })
		expect(obj.excludeNullProps({ a: 1, b: 2, c: 0 }))
			.toEqual({ a: 1, b: 2, c: 0 })
	})

	test('exclude empty properties', () => {
		expect(obj.excludeEmptyProps({ a: 1, b: 2, c: '' }))
			.toEqual({ a: 1, b: 2 })
		expect(obj.excludeEmptyProps({ a: 1, b: undefined, c: 'a' }))
			.toEqual({ a: 1, c: 'a' })
		expect(obj.excludeEmptyProps({ a: 1, b: null, c: 'a' }))
			.toEqual({ a: 1, c: 'a' })
		expect(obj.excludeEmptyProps({ a: 1, b: 2, c: 0 }))
			.toEqual({ a: 1, b: 2, c: 0 })
	})

	test('find property', () => {
		expect(obj.findProp({ a: 1, b: 2, c: 3 }, v => v === 2)).toBe(2)
		expect(obj.findProp({ a: 1, b: 2, c: 3 }, v => v === 4)).toBe(null)
	})

	test('contains another object', () => {
		expect(obj.contains({ a: 1, b: 2, c: 3 }, { a: 1, b: 2 })).toBe(true)
		expect(obj.contains({ a: 1, b: 2, c: 3 }, { a: 1, b: 3 })).toBe(false)
		expect(obj.contains({ a: 1, b: 2 },
			{ a: 1, b: 2, d: 4 } as { a: number, b: number }))
			.toBe(false)
		expect(obj.contains({ a: 1, b: 2, c: 3 }, { a: 1, b: 2, c: 3 })).toBe(true)
	})
})
