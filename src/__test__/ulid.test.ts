import { describe, expect, test } from 'vitest'
import * as ulid from '../ulid.js'

describe('ULID', () => {
	test('generate', () => {
		expect(ulid.createId()).toHaveLength(26)
		expect(ulid.createId(1)).toHaveLength(26)
	})

	test('minimal collision detection', () => {
		const ids: Record<string, number> = {}
		for (let i = 0; i < 1000; ++i) {
			const id = ulid.createId(1)
			expect(id in ids).toBe(false)
			ids[id] = 1
		}
	})

	test('generate monotonic', () => {
		const createId = ulid.monotonicFactory()
		expect(createId()).toHaveLength(26)
		expect(createId(1)).toHaveLength(26)
	})

	test('monotonic sortable', () => {
		const createId = ulid.monotonicFactory()
		let lastId = createId()
		for (let i = 0; i < 1000; ++i) {
			const id = createId()
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('monotonic sortable', () => {
		const createId = ulid.monotonicFactory()
		let lastId = createId(1)
		for (let i = 0; i < 1000; ++i) {
			const id = createId(1)
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('extreme time', () => {
		// cspell:disable
		expect(ulid.createId(8925 * 365 * 24 * 3600 * 1000))
			.toMatch('7ZZGXWNZ00')
		expect(() => ulid.createId(8926 * 365 * 24 * 3600 * 1000))
			.toThrow()
		// cspell:enable
	})
})
