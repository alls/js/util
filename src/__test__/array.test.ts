import { describe, expect, test } from 'vitest'
import * as array from '../array.js'

describe('utilities for arrays', () => {
	test('moveIn', () => {
		expect(array.moveIn([0, 1, 2, 3], 1, 2)).toEqual([0, 2, 1, 3])
		expect(array.moveIn([0, 1, 2, 3], 2, 1)).toEqual([0, 2, 1, 3])
		expect(array.moveIn([0, 1, 2, 3], 3, 1)).toEqual([0, 3, 1, 2])
		expect(array.moveIn([0, 1, 2, 3], 0, 2)).toEqual([1, 2, 0, 3])
		expect(array.moveIn([0, 1, 2, 3], 0, 3)).toEqual([1, 2, 3, 0])
		expect(array.moveIn([0, 1, 2, 3], 3, 0)).toEqual([3, 0, 1, 2])
		expect(array.moveIn([0, 1, 2, 3], 0, 4)).toEqual([1, 2, 3, 0])
		expect(array.moveIn([0, 1, 2, 3], 0, -1)).toEqual([1, 2, 3, 0])
		expect(array.moveIn([0, 1, 2, 3], -1, 1)).toEqual([0, 3, 1, 2])
	})

	test('remove', () => {
		[
			[1, [0, 2, 3]],
			[2, [0, 1, 3]],
			[3, [0, 1, 2]],
			[0, [1, 2, 3]],
			[-1, [0, 1, 2, 3]],
			[4, [0, 1, 2, 3]],
		].forEach(([idx, exp]) => {
			const arr = [0, 1, 2, 3]
			expect(array.remove(arr, idx)).toEqual(exp)
			expect(arr).toEqual([0, 1, 2, 3])
		})
	})

	test('removeIn', () => {
		[
			[1, [0, 2, 3]],
			[2, [0, 1, 3]],
			[3, [0, 1, 2]],
			[0, [1, 2, 3]],
			[-1, [0, 1, 2, 3]],
			[4, [0, 1, 2, 3]],
		].forEach(([idx, exp]) => {
			const arr = [0, 1, 2, 3]
			expect(array.removeIn(arr, idx)).toEqual(exp)
			expect(arr).toEqual(exp)
		})
	})

	test('range', () => {
		expect(array.range(0, 1)).toEqual([0, 1])
		expect(array.range(0, 3)).toEqual([0, 1, 2, 3])
		expect(array.range(1, 1)).toEqual([1])
		expect(array.range(-2, 1)).toEqual([-2, -1, 0, 1])
		expect(array.range(1, 0)).toEqual([])
		expect(array.range(3, 1)).toEqual([])
	})

	test('add', () => {
		expect(array.add([0, 1, 2], 3)).toEqual([0, 1, 2, 3])
		expect(array.add([0, 1, 2], 3, undefined)).toEqual([0, 1, 2, 3])
		expect(array.add([0, 1, 2], 3, 1)).toEqual([0, 3, 1, 2])
		expect(array.add([0, 1, 2], 3, -1)).toEqual([0, 1, 3, 2])
		expect(array.add([[0], [1, 2]], [3, 4], 1))
			.toEqual([[0], [3, 4], [1, 2]])
		const arr = [0, 1, 2]
		expect(array.add(arr, 1)).not.toBe(arr)
	})

	test('addAt', () => {
		expect(array.addAt([0, 1, 2], 3)).toEqual([0, 1, 2])
		expect(array.addAt([0, 1, 2], undefined, 3)).toEqual([0, 1, 2, 3])
		expect(array.addAt([0, 1, 2], 1, 3)).toEqual([0, 3, 1, 2])
		expect(array.addAt([0, 1, 2], -1, 3)).toEqual([0, 1, 3, 2])
		expect(array.addAt([0, 1, 2], -2, 3)).toEqual([0, 3, 1, 2])
		expect(array.addAt([0, 1, 2], 1, 3, 4)).toEqual([0, 3, 4, 1, 2])
		expect(array.addAt([[0], [1, 2]], 1, [3, 4]))
			.toEqual([[0], [3, 4], [1, 2]])
		const arr = [0, 1, 2]
		expect(array.addAt(arr, -1, 1)).not.toBe(arr)
	})

	test('move', () => {
		expect(array.move([0, 1, 2, 3], 1, 2)).toEqual([0, 2, 1, 3])
		expect(array.move([0, 1, 2, 3], 2, 1)).toEqual([0, 2, 1, 3])
		expect(array.move([0, 1, 2, 3], 3, 1)).toEqual([0, 3, 1, 2])
		expect(array.move([0, 1, 2, 3], 0, 2)).toEqual([1, 2, 0, 3])
		expect(array.move([0, 1, 2, 3], 0, 3)).toEqual([1, 2, 3, 0])
		expect(array.move([0, 1, 2, 3], 3, 0)).toEqual([3, 0, 1, 2])
		expect(array.move([0, 1, 2, 3], 0, 4)).toEqual([1, 2, 3, void 0, 0])
		expect(array.move([0, 1, 2, 3], 0, -1)).toEqual([1, 2, 3, 0])
		expect(array.move([0, 1, 2, 3], -1, 1)).toEqual([0, 3, 1, 2])
		const arr = [0, 1, 2, 3]
		expect(array.move(arr, 1, 2)).not.toBe(arr)
	})

	test('remove', () => {
		expect(array.remove([0, 1, 2, 3], 1)).toEqual([0, 2, 3])
		expect(array.remove([0, 1, 2, 3], 2)).toEqual([0, 1, 3])
		expect(array.remove([0, 1, 2, 3], 3)).toEqual([0, 1, 2])
		expect(array.remove([0, 1, 2, 3], 0)).toEqual([1, 2, 3])
		expect(array.remove([0, 1, 2, 3], -1)).toEqual([0, 1, 2, 3])
		const arr = [0, 1, 2, 3]
		expect(array.remove(arr, 1)).not.toBe(arr)
	})
})
