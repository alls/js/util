import { describe, expect, test } from 'vitest'
import * as num from '../num.js'

describe('utilities for numbers', () => {
	test('parse int', () => {
		expect(num.parseInt('1')).toBe(1)
		expect(num.parseInt('', 1)).toBe(1)
		expect(num.parseInt('0', 1)).toBe(0)
		expect(num.parseInt('null', 1)).toBe(1)
		expect(num.parseInt('1a')).toBe(1)
		expect(num.parseInt('a1a')).toBe(undefined)
		expect(num.parseInt('a1a', 2)).toBe(2)
	})

	test('parse numbers', () => {
		expect(num.parseNumbers('1')).toEqual([1])
		expect(num.parseNumbers('')).toEqual([])
		expect(num.parseNumbers('0')).toEqual([0])
		expect(num.parseNumbers('0,1,2')).toEqual([0, 1, 2])
		expect(num.parseNumbers('0, 1, 2')).toEqual([0, 1, 2])
		expect(num.parseNumbers('0, 1, , 2')).toEqual([0, 1, 2])
		expect(num.parseNumbers('0, 1, a , 2')).toEqual([0, 1, 2])
	})

	test('bigint JSON stringify', () => {
		expect(JSON.stringify({ a: 2n }, num.bigIntReplacer)).toEqual('{"a":"2n"}')
		expect(JSON.stringify(2n, num.bigIntReplacer)).toEqual('"2n"')
		expect(JSON.stringify(-2n, num.bigIntReplacer)).toEqual('"-2n"')
		expect(JSON.stringify(
			BigInt(Number.MAX_SAFE_INTEGER) + 10n, num.bigIntReplacer))
			.toEqual('"9007199254741001n"')
	})

	test('bigint JSON parse', () => {
		expect(JSON.parse('{"a":"2n"}', num.bigIntReviver)).toEqual({ a: 2n })
		expect(JSON.parse('"2n"', num.bigIntReviver)).toEqual(2n)
		expect(JSON.parse('"+2n"', num.bigIntReviver)).toEqual(2n)
		expect(JSON.parse('"-2n"', num.bigIntReviver)).toEqual(-2n)
		expect(JSON.parse('"9007199254741001n"', num.bigIntReviver))
			.toEqual(BigInt(Number.MAX_SAFE_INTEGER) + 10n)
	})
})
