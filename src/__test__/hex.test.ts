import { describe, expect, test } from 'vitest'
import * as hex from '../hex.js'

describe('utilities for hex encoding', () => {
	const testData = {
		'': [],
		'00': [0],
		'01': [1],
		'0a': [10],
		'0f': [15],
		'0A': [10],
		'0F': [15],
		'ff': [255],
		'0000': [0, 0],
		'ff00': [255, 0],
		'ffff': [255, 255],
		'100000': [16, 0, 0],
	}

	test('hex to bytes', () => {
		Object.entries(testData).forEach(([str, bytes]) => {
			expect(hex.decode(str)).toEqual(new Uint8Array(bytes))
		})
	})

	test('bytes to hex', () => {
		Object.entries(testData).forEach(([str, bytes]) => {
			expect(hex.encode(new Uint8Array(bytes))).toEqual(str.toLowerCase())
		})
	})

	test('invalid hex strings', () => {
		expect(() => hex.decode('0')).toThrow()
		expect(() => hex.decode('000')).toThrow()
		expect(() => hex.decode('101')).toThrow()
		expect(() => hex.decode('1/')).toThrow()
		expect(() => hex.decode('1:')).toThrow()
		expect(() => hex.decode('1@')).toThrow()
		expect(() => hex.decode('1G')).toThrow()
		expect(() => hex.decode('1`')).toThrow()
		expect(() => hex.decode('1g')).toThrow()
	})
})
