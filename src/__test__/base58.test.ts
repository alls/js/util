import { describe, expect, test } from 'vitest'
import { base58 } from '../baseX.js'

describe('utilities for base58 encoding', () => {
	const testDataNumber = [
		// cspell:disable
		[0, '1'], [57, 'z'], [58, '21'], [59, '22'],
		[66, '29'], [67, '2A'], [68, '2B'], [90, '2Z'], [91, '2a'], [116, '31'],
		[58 ** 6 * 2 - 1, '2zzzzzz'],
		[2147483647, '4GmR58'],
		[Number.MAX_SAFE_INTEGER - 1, '2DLNrMSKuf'],
		[Number.MAX_SAFE_INTEGER, '2DLNrMSKug'],
		// cspell:enable
	]

	test('encode Base58', () => {
		for (const [num, str] of testDataNumber)
			expect(base58.encode(num as number)).toEqual(str)
	})

	test('decode Base58', () => {
		for (const [num, str] of testDataNumber)
			expect(base58.decodeNumber(str as string)).toEqual(num)
	})

	const testDataBytes = [
		// cspell:disable
		[new Array(16).fill(0), new Array(22).fill('1').join('')],
		[[0x00], '11'], [[0x01], '12'], [[57], '1z'], [[58], '21'],
		[[115], '2z'], [[116], '31'],
		[[0xff], '5Q'], [[0x01, 0x00], '15R'],
		[[58 * 58 / 256, 58 * 58 % 256 - 1], '1zz'],
		[[58 * 58 / 256, 58 * 58 % 256], '211'],
		[[0xff, 0], 'LQX'], [[0xff, 0xff], 'LUv'],
		[[0x74, 0x65, 0x73, 0x74], '3yZe7d'],
		[[0x74, 0x65, 0x73, 0x74, 0x01], 'E8f4pDv'],
		[[0x00, 0x11, 0x22, 0x33, 0x44, 0x55], '12w7jumW'],
		[
			[0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33, 0x14, 0x18, 0xb6,
				0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4],
			'KbvUzjituucqo1K8Rr5gvsEBqSB',
		],
		// cspell:enable
	]

	test('encode buffer as Base58', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base58.encode(bytes as number[])).toEqual(str)
	})

	test('decode buffer as Base58', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base58.decode(str as string))
				.toEqual(new Uint8Array(bytes as number[]))
	})

	test('increment Base58', () => {
		expect(() => base58.increment('0')).toThrow()
		expect(base58.increment('1')).toEqual('2')
		expect(base58.increment('9')).toEqual('A')
		expect(base58.increment('A')).toEqual('B')
		expect(base58.increment('Z')).toEqual('a')
		expect(base58.increment('a')).toEqual('b')
		expect(() => base58.increment('z')).toThrow()
		expect(base58.increment('1z')).toEqual('21')
		expect(base58.increment('1zz')).toEqual('211')
		expect(base58.increment('1zzz')).toEqual('2111')
		expect(() => base58.increment('zz')).toThrow()
		expect(() => base58.increment('zzz')).toThrow()
	})
})
