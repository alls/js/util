import { describe, expect, test } from 'vitest'
import * as ulid62 from '../ulid62.js'

const fixTime = ulid62.epochTime2020Millis + 10

describe('ULID62', () => {
	test('generate', () => {
		expect(ulid62.createId()).toHaveLength(21)
		expect(ulid62.createId(fixTime)).toHaveLength(21)
	})

	test('minimal collision detection', () => {
		const ids: Record<string, number> = {}
		for (let i = 0; i < 1000; ++i) {
			const id = ulid62.createId(fixTime)
			expect(id in ids).toBe(false)
			ids[id] = 1
		}
	})

	test('generate monotonic', () => {
		const createId = ulid62.monotonicFactory()
		expect(createId()).toHaveLength(21)
		expect(createId(fixTime)).toHaveLength(21)
	})

	test('monotonic sortable', () => {
		const createId = ulid62.monotonicFactory()
		let lastId = createId()
		for (let i = 0; i < 1000; ++i) {
			const id = createId()
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('monotonic sortable fix time', () => {
		const createId = ulid62.monotonicFactory()
		let lastId = createId(fixTime)
		for (let i = 0; i < 1000; ++i) {
			const id = createId(fixTime)
			expect(id > lastId, `${id} ${lastId}`).toBe(true)
			lastId = id
		}
	})

	test('extreme time', () => {
		const maxTime = ulid62.epochTime2020 * 1000 + 62 ** 7 * 10 - 1
		expect(ulid62.createId(maxTime)).toMatch('zzzzzzz')
		expect(() => ulid62.createId(maxTime + 1)).toThrow()
	})
})
