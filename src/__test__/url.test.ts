import { describe, expect, test } from 'vitest'
import * as url from '../url.js'
import { addParams } from '../url.js'

const imageUrls = [
	'https://d.tld/pic.jpg',
	'https://d.tld/pic.jpg?some=query',
	'https://d.tld/pic.jpg#hash',
	'https://d.tld/pic.jpg?some=query#hash',
	'data:image/gif;base64,R0lGODlhEA',
]

const htmlUrls = [
	'https://d.tld/page.html',
	'https://d.tld/page.html?some=query',
	'https://d.tld/page.html#hash',
	'https://d.tld/page.html?some=query#hash',
	'https://d.tld/folder.jpg/page.html',
	'https://d.tld/page.html?some=query.jpg',
	'https://d.tld/page.html?some=query#hash.jpg',
	'data:text/html;<img src="https://d.tld/pic.jpg">',
]

const dataUrls = [
	'data:,abc',
	'data:abc',
	'data:text/plain,abc',
	'data:image/png,abc',
	'data:text/plain;base64,abc',
	'data:image/png;base64,abc',
	'data:image/svg+xml,abc',
	'data:image/svg+xml;utf8,<svg>',
	'data:text/html;<img src="https://d.tld/pic.jpg">',
]

const invalidUrls = [
	'https://d.tdl/ pic.jpg',
]

describe('utilities for URLs', () => {
	test('is URLs', () => {
		for (const u of imageUrls)
			expect(url.isUrl(u)).toBe(true)
		for (const u of htmlUrls)
			expect(url.isUrl(u)).toBe(true)
		for (const u of invalidUrls)
			expect(url.isUrl(u)).toBe(false)
	})

	test('image URLs', () => {
		const t = url.pattern.line.image.test.bind(url.pattern.line.image)
		for (const u of imageUrls)
			expect(t(u)).toBe(true)
		for (const u of htmlUrls)
			expect(t(u)).toBe(false)
		for (const u of invalidUrls)
			expect(t(u)).toBe(false)
	})

	describe('addParams', () => {
		test('return the url when params are not passed', () => {
			const url = 'https://example.com'

			const result = addParams(url)

			expect(result).toBe(url)
		})

		test('add params to the URL', () => {
			const url = 'https://example.com'
			const params = {
				param1: 'value1',
				param2: 'value2',
			}

			const result = addParams(url, params)

			expect(result).toBe(`${url}/?param1=value1&param2=value2`)
		})

		test('add params to the URL with path', () => {
			const url = 'https://example.com/path'
			const params = {
				param1: 'value1',
				param2: 'value2',
			}

			const result = addParams(url, params)

			expect(result).toBe(`${url}?param1=value1&param2=value2`)
		})

		test('add and overwrite params to the URL', () => {
			const url = 'https://example.com'
			const params = {
				param1: 'value1',
				param2: 'value2',
			}

			const result = addParams(url + '/?param2=value0', params)

			expect(result).toBe(`${url}/?param1=value1&param2=value2`)
		})

		test('add multiple values for the same key', () => {
			const url = 'https://example.com'
			const params = {
				param1: ['value1', 'value2'],
			}

			const result = addParams(url, params)

			expect(result).toBe(`${url}/?param1=value1&param1=value2`)
		})

		test('remove params with undefined or empty values', () => {
			const url = 'https://example.com'
			const params = {
				param1: 'value1',
				param2: undefined,
				param3: '',
			}

			const result = addParams(url, params)

			expect(result).toBe(`${url}/?param1=value1`)
		})

		test('add values to a URL path only', () => {
			const url = '/root'
			const params = {
				param1: ['value1', 'value2'],
			}

			const result = addParams(url, params)

			expect(result).toBe(`${url}?param1=value1&param1=value2`)
		})
	})

	describe('Data URLs', () => {
		test('is data URL', () => {
			for (const u of dataUrls)
				expect(url.isDataUrl(u)).toBe(true)
			for (const u of htmlUrls.filter(u => !u.startsWith('data:')))
				expect(url.isDataUrl(u)).toBe(false)
			for (const u of imageUrls.filter(u => !u.startsWith('data:')))
				expect(url.isDataUrl(u)).toBe(false)
			for (const u of invalidUrls)
				expect(url.isDataUrl(u)).toBe(false)
		})

		test('data URL media type', () => {
			expect(dataUrls.map(u => url.dataUrlMediaType(u))).toMatchInlineSnapshot(`
				[
				  "text/plain",
				  "text/plain",
				  "text/plain",
				  "image/png",
				  "text/plain",
				  "image/png",
				  "image/svg+xml",
				  "image/svg+xml",
				  "text/plain",
				]
			`)
		})

		test('data URL to node Buffer', () => {
			expect(dataUrls.map(u => url.dataUrlToBuffer(u))).toMatchInlineSnapshot(`
				[
				  {
				    "data": [
				      97,
				      98,
				      99,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      97,
				      98,
				      99,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      97,
				      98,
				      99,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      97,
				      98,
				      99,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      105,
				      183,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      105,
				      183,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      97,
				      98,
				      99,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      60,
				      115,
				      118,
				      103,
				      62,
				    ],
				    "type": "Buffer",
				  },
				  {
				    "data": [
				      116,
				      101,
				      120,
				      116,
				      47,
				      104,
				      116,
				      109,
				      108,
				      59,
				      60,
				      105,
				      109,
				      103,
				      32,
				      115,
				      114,
				      99,
				      61,
				      34,
				      104,
				      116,
				      116,
				      112,
				      115,
				      58,
				      47,
				      47,
				      100,
				      46,
				      116,
				      108,
				      100,
				      47,
				      112,
				      105,
				      99,
				      46,
				      106,
				      112,
				      103,
				      34,
				      62,
				    ],
				    "type": "Buffer",
				  },
				]
			`)
		})
	})
})
