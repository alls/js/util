import { describe, expect, test } from 'vitest'
// import * as base62 from '../base62.js'
import { base62 } from '../baseX.js'

describe('utilities for base62 encoding', () => {
	const testDataNumber = [
		// cspell:disable
		[0, '0'], [61, 'z'], [62, '10'], [63, '11'],
		[71, '19'], [72, '1A'], [73, '1B'], [97, '1Z'], [98, '1a'], [124, '20'],
		[62 ** 6 * 2 - 1, '1zzzzzz'],
		[2147483647, '2LKcb1'],
		[Number.MAX_SAFE_INTEGER - 1, 'fFgnDxSe6'],
		[Number.MAX_SAFE_INTEGER, 'fFgnDxSe7'],
		// cspell:enable
	]

	test('encode Base62', () => {
		for (const [num, str] of testDataNumber)
			expect(base62.encode(num as number)).toEqual(str)
	})

	test('decode Base62', () => {
		for (const [num, str] of testDataNumber)
			expect(base62.decodeNumber(str as string)).toEqual(num)
	})

	const testDataBytes = [
		// cspell:disable
		[new Array(16).fill(0), new Array(22).fill('0').join('')],
		[[0x00], '00'], [[0x01], '01'], [[61], '0z'], [[62], '10'],
		[[123], '1z'], [[124], '20'],
		[[0xff], '47'], [[0x01, 0x00], '048'],
		[[1, 0], '048'],
		[[62 * 62 / 256, 62 * 62 % 256 - 1], '0zz'],
		[[62 * 62 / 256, 62 * 62 % 256], '100'],
		[[0xff, 0], 'Gyu'], [[0xff, 0xff], 'H31'],
		[[0x74, 0x65, 0x73, 0x74], '289lyu'],
		[[0x74, 0x65, 0x73, 0x74, 0x01], '8ngM7TF'],
		[[0x00, 0x11, 0x22, 0x33, 0x44, 0x55], '01IK8i2f'],
		[
			[0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33, 0x14, 0x18, 0xb6,
				0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4],
			'3HegXcF2kstxTdbx2lkH0KL1Y04',
		],
		// cspell:enable
	]

	test('encode buffer as Base62', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base62.encode(bytes as number[])).toEqual(str)
	})

	test('decode buffer as Base62', () => {
		for (const [bytes, str] of testDataBytes)
			expect(base62.decode(str as string))
				.toEqual(new Uint8Array(bytes as number[]))
	})

	test('increment Base62', () => {
		expect(base62.increment('0')).toEqual('1')
		expect(base62.increment('9')).toEqual('A')
		expect(base62.increment('A')).toEqual('B')
		expect(base62.increment('Z')).toEqual('a')
		expect(base62.increment('a')).toEqual('b')
		expect(base62.increment('0z')).toEqual('10')
		expect(base62.increment('0zz')).toEqual('100')
		expect(base62.increment('0zzz')).toEqual('1000')
		expect(() => base62.increment('z')).toThrow()
		expect(() => base62.increment('zz')).toThrow()
		expect(() => base62.increment('zzz')).toThrow()
	})
})
