// https://stackoverflow.com/a/58765199
export type OmitFirstArg<F> =
	F extends (x: any, ...args: infer P) => infer R ? (...args: P) => R : never

export type Primitive = string | number | boolean

export type Optional<T, K extends keyof T> = Omit<T, K> & Partial<T>

export type Nullable<T> = T | null | undefined
