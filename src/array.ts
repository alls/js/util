
/** Create new array with one additional element.
 * Undefined index means append. Negative index means count from the end.
 */
export function add<T>(arr: T[] | undefined | null, e: T, index?: number) {
	if (!arr)
		return [e]
	const copy = arr.slice()
	if (typeof index !== 'number')
		copy.push(e)
	else
		copy.splice(index, 0, e)
	return copy
}

/** Create new array with additional elements at the given index.
 * Undefined index means append. Negative index means count from the end.
 */
export function addAt<T>(arr: T[] | undefined | null,
	index: number | undefined, ...e: T[]) {
	if (!arr)
		return e
	const copy = arr.slice()
	if (typeof index !== 'number')
		copy.push(...e)
	else
		copy.splice(index, 0, ...e)
	return copy
}

/**
 * Create new array with the first found element removed.
 * Like Array.filter, but only the first matching element.
 * @param arr Array to remove element from.
 * @param elem The element to remove.
 * @returns New array without the first given element.
 */
export function removeFirst<T>(arr: T[] | undefined | null, elem?: T) {
	if (!arr)
		return []
	if (elem === undefined)
		return arr
	const idx = arr.indexOf(elem)
	if (idx < 0)
		return arr
	const newArr = arr.slice()
	newArr.splice(idx, 1)
	return newArr
}

/**
 * Create new array with all found elements removed.
 * Array.filter(e => e !== elem).
 * @param arr Array to remove element from.
 * @param elem The element to remove.
 * @returns New array without the first given element.
 */
export function remove<T>(arr: T[] | undefined | null, elem?: T) {
	if (!arr)
		return []
	return arr.filter(e => e !== elem)
}

/**
 * Remove first found element from array.
 * Array.indexOf and Array.splice(idx, 1).
 * @param arr Array to remove element from.
 * @param elem The element to remove.
 * @returns The mutated input array (arr).
 */
export function removeFirstIn<T>(arr: T[] | undefined | null, elem?: T) {
	if (!arr)
		return []
	if (elem === undefined)
		return arr
	const idx = arr.indexOf(elem)
	if (idx >= 0)
		arr.splice(idx, 1)
	return arr
}

/**
 * Remove all found elements from array.
 * While Array.indexOf do Array.splice(idx, 1).
 * @param arr Array to remove elements from.
 * @param elem The element to remove.
 * @returns The mutated input array (arr).
 */
export function removeIn<T>(arr: T[] | undefined | null, elem?: T) {
	if (!arr)
		return []
	if (elem === undefined)
		return arr
	while (true) {
		const idx = arr.lastIndexOf(elem)
		if (idx < 0)
			break
		arr.splice(idx, 1)
	}
	return arr
}

/**
 * Create new array with all found elements replaced.
 * @param arr Array to replace elements from.
 * @param elem The element to replace.
 * @param withElem The element to be replaced with.
 * @returns New array with the element replaced.
 */
export function replace<T>(arr: T[], elem: T, withElem: T) {
	return arr.map(e => e === elem ? withElem : e)
}

/**
 * Create new array with an element moved.
 * @param arr Array to move the element in.
 * @param fromIdx Index of the element to move.
 * @param toIdx Target index to move the element to.
 * @returns New array with the element moved.
 */
export function move<T>(arr: T[], fromIdx: number, toIdx: number) {
	if (fromIdx < 0)
		fromIdx = arr.length + fromIdx
	if (toIdx < 0)
		toIdx = arr.length + toIdx
	const res = arr.slice()
	const target = arr[fromIdx]
	const inc = fromIdx > toIdx ? -1 : 1
	for (let i = fromIdx; i !== toIdx; i += inc)
		res[i] = arr[i + inc]
	res[toIdx] = target
	return res
}

/**
 * Move an element within an array.
 * @param arr Array to move the element in.
 * @param fromIdx Index of the element to move.
 * @param toIdx Target index to move the element to.
 * @returns The mutated input array (arr).
 */
export function moveIn<T>(arr: T[], fromIdx: number, toIdx: number) {
	if (fromIdx < 0)
		fromIdx = arr.length + fromIdx
	if (fromIdx >= arr.length)
		fromIdx = arr.length - 1
	if (toIdx < 0)
		toIdx = arr.length + toIdx
	if (toIdx >= arr.length)
		toIdx = arr.length - 1
	const target = arr[fromIdx]
	const inc = fromIdx > toIdx ? -1 : 1
	for (let i = fromIdx; i !== toIdx; i += inc)
		arr[i] = arr[i + inc]
	arr[toIdx] = target
	return arr
}

/**
 * Array filled with a range of numbers. Step 1, end inclusive.
 * @param start Start number at index 0 of the result array.
 * @param end End number at the last index of the result array.
 * @returns Number array.
 */
export function range(start: number, end: number) {
	if (start > end) return []
	const len = end - start + 1
	const arr = new Array<number>(len)
	for (let i = 0; i < len; ++i) arr[i] = i + start
	return arr
}

export function toObject<T extends string | number>(arr: T[])
	: { [k in T]: T }
export function toObject<T extends string | number, V>(arr: T[], val?: V)
	: { [k in T]: V }
export function toObject<T, K extends string | number>(arr: T[],
	keyFn: (v: T, idx: number) => K): { [k in K]: T }
export function toObject<T, K extends string | number, V>(arr: T[],
	keyFn: (v: T, idx: number) => K,
	val: V | ((v: T, idx: number) => V)): { [k in K]: V }
export function toObject(arr: unknown[],
	keyFn?: (v: unknown, idx: number) => string | number,
	val?: unknown | ((v: unknown, idx: number) => unknown)) {
	const obj: Record<string, unknown> = {}
	if (!arr)
		return obj
	if (typeof keyFn === 'function') {
		for (let i = 0, len = arr.length; i < len; ++i) {
			const v = arr[i]
			const k = keyFn(v, i)
			obj[k] = typeof val === 'function' ? val(v, i) as unknown : val ?? v
		}
	} else {
		for (const v of arr) {
			const k = String(v)
			obj[k] = keyFn ?? v
		}
	}
	return obj
}

/**
 * Add a given value to an array of values only if it is not contained already.
 * 
 * @param values Array of values to add the given value to.
 * @param value Value to add.
 * @returns New array of values including the given value.
 */
export function addUnique<V>(values: V[] | undefined | null, value: V) {
	if (!values)
		return [value]
	if (values.indexOf(value) < 0)
		return [...values, value]
	return values
}

/**
 * Add a given value to an array of values only if it is not contained already.
 * 
 * If the given array does not yet exist, a new array is returned.
 * 
 * @param values Array of values to add the given value to.
 * @param value Value to add.
 * @returns Given or new array of values including the given value.
 */
export function addUniqueIn<V>(values: V[] | undefined | null, value: V) {
	if (!values)
		return [value]
	if (values.indexOf(value) < 0)
		values.push(value)
	return values
}

