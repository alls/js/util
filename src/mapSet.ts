import * as array from './array.js'

export function add<V>(mapSet: { [key: string]: V[] }, key: string, value: V) {
	mapSet[key] = key in mapSet
		? array.addUnique(mapSet[key], value)
		: [value]
	return mapSet
}

export function remove<V>(mapSet: { [key: string]: V[] },
	key: string, value: V) {
	if (key in mapSet) {
		const arr = array.remove(mapSet[key], value)
		if (arr.length < 1)
			delete mapSet[key]
	}
	return mapSet
}

