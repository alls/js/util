import type { Primitive } from './types.js'

export const pattern = {
	// complete lines
	line: {
		basic: /^https?:\/\/\S+$/,
		image:
			/^(https?:\/\/[^ #\?]+\.(png|gif|jpg|jpeg)|data:image\/.+)([#\?].*)?$/,
	},
	// within text
	basic: /(https?:\/\/\S+)/,
	image:
		/(https?:\/\/[^ #\?]+\.(png|gif|jpg|jpeg)|data:image\/.+)([#\?].*)?(?:\s|$)/,
}

export function isUrl(val: any) {
	return !!val && (val instanceof URL ||
		(typeof val === 'string' &&
			(pattern.line.basic.test(val) || isDataUrl(val))))
}

export function containsUrl(val: string) {
	return val && pattern.basic.test(val)
}

/** Test a URL to be a data URL.
 * Optionally include the encoding (eg. base64,...) in the test.
 */
export function isDataUrl(v: any, encoding?: string): v is string {
	return typeof v === 'string' && v.startsWith('data:') &&
		(!encoding || encoding === v.substring(v.indexOf(';') + 1, v.indexOf(',')))
}


/** Extracts the media type of a data URL. */
export function dataUrlMediaType(url: string) {
	if (!url.startsWith('data:'))
		throw new Error(`Invalid data URL! ${url}`)
	const dataIdx = url.indexOf(',', 5)
	const encodingIdx = url.indexOf(';', 5)
	return dataIdx > 5
		? encodingIdx === 5
			? 'text/plain'
			: url.substring(5,
				encodingIdx > 5 && encodingIdx < dataIdx ? encodingIdx : dataIdx)
		: 'text/plain'
}

/** Build a Buffer from a data URL.
 * So far only supports base64 or utf8 encoding.
 */
export function dataUrlToBuffer(url: string) {
	if (!url.startsWith('data:'))
		throw new Error(`Invalid data URL! ${url}`)
	url = url.substring(5)
	const dataIdx = url.indexOf(',')
	if (dataIdx >= 0) {
		const encodingIdx = url.indexOf(';base64')
		return Buffer.from(url.substring(dataIdx + 1),
			encodingIdx >= 0 && encodingIdx < dataIdx ? 'base64' : 'utf8')
	}
	return Buffer.from(url, 'utf8')
}
/**
 * Add, remove and overwrite query parameters of an URL or URL path.
 * 
 * @param url URL or URL path to add parameters to.
 * @param params Key/values of parameters to add.
 * @param defaults Key/values of default values to remove.
 * @returns URL string with query parameters.
 */
export function addParams(url: string,
	params?: Record<string, Primitive | undefined | (Primitive | undefined)[]>,
	defaults?: Record<string, Primitive>) {
	if (!url)
		return url
	const queryIdx = url.indexOf('?')
	const query = new URLSearchParams(queryIdx >= 0
		? url.substring(queryIdx)
		: '')
	if (params) {
		for (const k of Object.keys(params)) {
			const v = params[k]
			if (Array.isArray(v)) {
				const arr = v.filter(Boolean)
				query.delete(k)
				for (const v of arr)
					if (v !== undefined)
						query.append(k, v.toString())
			} else if (!v) {
				query.delete(k)
			} else {
				query.set(k, v.toString())
			}
		}
	}
	if (defaults) {
		for (const k of Object.keys(defaults)) {
			if (query.get(k) == defaults[k])
				query.delete(k)
		}
	}
	query.sort()
	const main = queryIdx < 0 ? url : url.substring(0, queryIdx)
	const domainStart = main.indexOf('://')
	const search = query.toString()
	return search
		? domainStart > 0
			? main.indexOf('/', domainStart + 4) >= 0
				? `${main}?${search}`
				: `${main}/?${search}`
			: `${main}?${search}`
		: main
}

export function absolute(url: string, base: string) {
	return new URL(url, base).href
}
