
const stringToValue: Record<string, boolean> = {
	'true': true,
	'1': true,
	'yes': true,
	'false': false,
	'0': false,
	'no': false,
}

/**
 * Parse boolean string.
 * @param str String to be parsed as boolean.
 * @param defaultValue Default value if string is not a boolean.
 * @returns Parsed boolean or default value.
 */
function parse(str: string | null | undefined): boolean | undefined
function parse(str: string | null | undefined, defaultValue: boolean): boolean
function parse(str: string | null | undefined, defaultValue?: boolean) {
	if (str === null || str === undefined) return defaultValue
	const v = str.toLowerCase()
	return v in stringToValue ? stringToValue[v] : defaultValue
}
export { parse }
