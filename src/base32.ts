import { replaceCharAt } from './str.js'

export interface Alphabet {
	encode: string[]
	decode: { [char: string]: number }
}

export const alphabet = {
	// cspell:disable
	// https://www.crockford.com/base32.html
	crockford: {
		encode: '0123456789ABCDEFGHJKMNPQRSTVWXYZ'.split(''),
		decode: { 'O': 0, 'L': 1, 'I': 1, 'o': 0, 'l': 1, 'i': 1 }
	} as Alphabet,
	// similar to Crockford. optimized for lower case. u instead of s.
	// https://github.com/agnoster/base32-js
	crockford2: {
		encode: '0123456789abcdefghjkmnpqrtuvwxyz'.split(''),
		decode: { 'O': 0, 'L': 1, 'I': 1, 'S': 5, 'o': 0, 'l': 1, 'i': 1, 's': 5 }
	} as Alphabet,
	// cspell:enable
}
alphabet.crockford.encode.forEach((c, i) => {
	alphabet.crockford.decode[c] = i
	alphabet.crockford.decode[c.toLowerCase()] = i
})
alphabet.crockford2.encode.forEach((c, i) => {
	alphabet.crockford2.decode[c] = i
	alphabet.crockford2.decode[c.toLowerCase()] = i
})

/** 
 * Encode positive integer (0..2147483647) or byte array data to base32 string
 * (eg. '0'..'1zzzzzz').
*/
export function encode(data: number | ArrayBufferLike,
	alphabetSpec: Alphabet | Alphabet['encode'] = alphabet.crockford2) {
	const alpha = 'encode' in alphabetSpec ? alphabetSpec.encode : alphabetSpec
	let str = ''
	if (typeof data === 'number') {
		if (!Number.isInteger(data))
			throw new Error(`${data} is not an integer direct base32 encoding!`)
		if (data == 0)
			return '0'
		if (data === data >> 0) {
			data = data >> 0
			while (data > 0) {
				str = alpha[data & 0x1f] + str
				data = data >> 5
			}
		} else {
			while (data > 0) {
				str = alpha[data % 0x20] + str
				data = Math.floor(data / 0x20)
			}
		}
	} else {
		const view = new DataView(data instanceof Uint8Array ? data.buffer : data)
		let bits = 0, v = 0
		for (let i = view.byteLength - 1; i >= 0; --i) {
			v = v | view.getUint8(i) << bits
			bits += 8
			while (bits >= 5) {
				str = alpha[v & 0x1f] + str
				v = v >>> 5
				bits -= 5
			}
		}
		if (bits > 0)
			str = alpha[v & 0x1f] + str
	}
	return str
}

export function decodeNumber(str: string,
	alphabetSpec: Alphabet | Alphabet['decode'] = alphabet.crockford2) {
	const alpha = ('decode' in alphabetSpec
		? alphabetSpec.decode : alphabetSpec) as Alphabet['decode']
	let num = 0
	for (const c of str) {
		num = num * 0x20
		num += alpha[c]
	}
	return num
}

export function decode(str: string,
	alphabetSpec: Alphabet | Alphabet['decode'] = alphabet.crockford2) {
	const alpha = ('decode' in alphabetSpec
		? alphabetSpec.decode : alphabetSpec) as Alphabet['decode']
	const len = Math.floor(str.length * 5 / 8)
	const arr = new Uint8Array(len)
	let idx = len - 1
	let bits = 0
	let v = 0
	for (let i = str.length - 1; i >= 0; --i) {
		v = v | alpha[str[i]] << bits
		bits += 5
		if (bits >= 8) {
			arr[idx--] = v & 0xff
			v = v >>> 8
			bits -= 8
		}
	}
	if (bits > 0)
		arr[idx--] = v & 0xff
	return arr
}

export function increment(str: string,
	alphabetSpec: Alphabet | Alphabet['encode'] = alphabet.crockford2) {
	const alpha = 'encode' in alphabetSpec ? alphabetSpec.encode : alphabetSpec
	const maxIdx = alpha.length - 1
	for (let i = str.length - 1; i >= 0; --i) {
		const c = str[i]
		const idx = alpha.indexOf(c)
		if (idx < 0)
			throw new Error(`Invalid base32 string '${str}'!`)
		if (idx === maxIdx)
			str = replaceCharAt(str, i, alpha[0])
		else
			return replaceCharAt(str, i, alpha[idx + 1])
	}
	throw new Error(`Base32 string '${str}' cannot be incremented!`)
}

