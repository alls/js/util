import createBaseX from './3rdParty/base-x.js'
import { replaceCharAt } from './str.js'

export interface Alphabet {
	encode: string[]
	decode: { [char: string]: number }
}

const base62charset =
	'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
export const alphabet: Alphabet = {
	encode: base62charset.split(''),
	decode: {},
}
alphabet.encode.forEach((c, i) => {
	alphabet.decode[c] = i
})
const baseX62 = createBaseX(base62charset)

// TODO: try to merge with base32

/** 
 * Encode positive integer (0..9007199254740991 (2^53 − 1)) or
 * byte array data to base62 string
 * (eg. '0'..'fFgnDxSe7').
*/
export function encode(data: number | ArrayLike<number> | ArrayBufferLike,
	alpha = alphabet) {
	const base = alpha.encode.length
	let str = ''
	if (typeof data === 'number') {
		if (!Number.isInteger(data))
			throw new Error(`${data} is not an integer for base62 encoding!`)
		if (data == 0)
			return alpha.encode[0]
		while (data > 0) {
			str = alpha.encode[data % base] + str
			data = Math.floor(data / base)
		}
	} else {
		const arr = data instanceof Uint8Array ? data : new Uint8Array(data)
		const coder =
			alpha === alphabet ? baseX62 : createBaseX(alphabet.encode.join(''))
		str = coder.encode(arr)
			.padStart(Math.ceil(arr.length * 8 / 6), alpha.encode[0])
	}
	return str
}

export function decodeNumber(str: string, alpha = alphabet) {
	const base = alpha.encode.length
	let num = 0
	for (const c of str) {
		num = num * base
		num += alpha.decode[c]
	}
	return num
}

export function decodeBigInt(str: string, alpha = alphabet) {
	const base = BigInt(alpha.encode.length)
	let v = 0n
	for (const c of str)
		v = v * base + BigInt(alpha.decode[c])
	return v
}

export function decode(str: string, alpha = alphabet) {
	const coder =
		alpha === alphabet ? baseX62 : createBaseX(alphabet.encode.join(''))
	const arr = coder.decode(str)
	const len = Math.floor(str.length * 6 / 8)
	if (arr.length > len)
		return arr.slice(arr.length - len)
	return arr
}

export function bytesToBigInt(arr: Uint8Array) {
	let v = 0n
	for (const b of arr)
		v = (v << 8n) + BigInt(b)
	return v
}

export function bigIntToBytes(v: bigint, arr: Uint8Array) {
	for (let i = arr.length - 1; i >= 0; --i) {
		arr[i] = Number(v % 256n)
		v = v / 256n
	}
	return arr
}

export function increment(str: string, alpha = alphabet) {
	const base = alpha.encode.length
	const maxIdx = base - 1
	for (let i = str.length - 1; i >= 0; --i) {
		const c = str[i]
		const idx = alpha.decode[c]
		if (idx < 0)
			throw new Error(`Invalid base${base} string '${str}'!`)
		if (idx === maxIdx)
			str = replaceCharAt(str, i, alpha.encode[0])
		else
			return replaceCharAt(str, i, alpha.encode[idx + 1])
	}
	throw new Error(`Base${base} string '${str}' cannot be incremented!`)
}

