
export function encode(str: string): string {
	return str
		? typeof self !== 'undefined' && typeof self.btoa === 'function'
			? self.btoa(str)
			: Buffer.from(str).toString('base64')
		: ''
}

export function decode(base64: string): string {
	return base64
		? typeof self !== 'undefined' && typeof self.atob === 'function'
			? self.atob(base64)
			: Buffer.from(base64, 'base64').toString('utf-8')
		: ''
}

