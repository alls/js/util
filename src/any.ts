import * as date from './date.js'
import * as error from './error.js'

/** Returns true if the given value v is true. Reusable v => !!v */
export function isTrue<T>(v: T | null | undefined): v is T { return !!v }

/** Returns true if the given value v is true. Reusable v => !v */
export function isFalse<T>(v: T): boolean { return !v }

export function isPrimitive(v: any): v is string | number | boolean {
	return v !== Object(v)
}

export function isNumber(v: any): v is number {
	return typeof v === 'number'
}

export function stringify(val: any,
	indent?: string, maxLevels = 100, stringQuote = "'") {
	return _stringify(val, 0, indent ?? null, maxLevels, stringQuote)
}

function _stringify(val: unknown, level: number, indent: string | null,
	maxLevels: number, quote: string): string {
	if (level > maxLevels)
		return '-- stringify: maximal level depth exceeded! --'
	const t = typeof val
	if (t === 'function')
		return ''
	if (t === 'string')
		return `${quote}${val as string}${quote}`
	if (t !== 'object')
		return String(val)
	if (val === null)
		return ''
	if (date.isDate(val))
		return quote + val.toISOString() + quote
	if (val instanceof Error)
		return _stringify(error.toJson(val), level, indent, maxLevels, quote)
	if (Object.prototype.toString.call(val) === '[object Array]' &&
		'map' in (val as Array<unknown>))
		return `[${(val as Array<unknown>)
			.map(v => _stringify(v, level + 1, indent, maxLevels, quote))
			.join(indent ? ', ' : ',')}]`
	const keys: string[] = []
	for (const k in (val as object))
		keys.push(k)
	if (keys.length <= 0)
		return '{}'
	keys.sort()
	const members = []
	for (const k of keys) {
		const s = _stringify((val as object)[k as keyof typeof val],
			level + 1, indent, maxLevels,
			quote)
		if (s)
			members.push(`${k}:${indent ? ' ' : ''}${s}`)
	}
	const nl = indent ? `\n${indent.repeat(level + 1)}` : ''
	return `{${nl}${members.join(',' + nl)}${indent ?
		`\n${indent.repeat(level)}` : ''}}`
}

/** Deep clone the data of any value (object, array,...).
 * Primitive, functions, symbols, and other non-serializable
 * values are not cloned.
 * @param v Value to clone.
 * @returns Cloned value.
 */
export function clone<T>(v: T): T {
	if (v === null || typeof v !== 'object')
		return v
	if (v instanceof Date)
		return new Date(v.getTime()) as T
	if (v instanceof Array)
		return v.map(clone) as T
	if (v instanceof Object) {
		const objCopy = {} as { [key: string]: any }
		for (const key in v) {
			if (v.hasOwnProperty(key))
				objCopy[key] = clone(v[key])
		}
		return objCopy as T
	}
	return v
}
