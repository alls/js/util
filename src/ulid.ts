import * as base32 from './base32.js'
import { getRandomValues } from './crypto.js'

/**
 * Simple ULID (https://github.com/ulid/spec) implementation.
 *
 * Unfortunately the ULID JavaScript repo is not working out of the box and
 * seems abandoned (https://github.com/ulid/javascript/issues/72) and the more
 * recent fork https://github.com/perry-mitchell/ulidx does not support ESM
 * imports.
 */
export function createId(seedTime = Date.now()) {
	const random = getRandomValues(10)
	return encodeTime(seedTime) + base32.encode(random, base32.alphabet.crockford)
}

export function monotonicFactory() {
	let time = 0
	let random: string
	return function createId(seedTime = Date.now()) {
		if (seedTime <= time) {
			random = base32.increment(random, base32.alphabet.crockford)
		} else {
			random = base32.encode(getRandomValues(10), base32.alphabet.crockford)
			time = seedTime
		}
		return encodeTime(time) + random
	}
}

const maxTime = Math.pow(2, 48) - 1

function encodeTime(time: number) {
	if (time < 0 || time > maxTime)
		throw new Error(`Invalid time value ${time}!`)
	return base32.encode(time, base32.alphabet.crockford).padStart(10, '0')
}

