
export function decode(hexString: string) {
	if (typeof hexString !== 'string' || hexString.length % 2 !== 0)
		throw new Error(`Invalid hex string '${hexString}' to convert to bytes!`)
	const len = hexString.length / 2
	const arr = new Uint8Array(len)
	for (let i = 0; i < len; ++i) {
		const v1 = codeToNumber(hexString.charCodeAt(i * 2))
		const v2 = codeToNumber(hexString.charCodeAt(i * 2 + 1))
		if (v1 < 0 || v1 > 15 || v2 < 0 || v2 > 15)
			throw new Error(`Invalid hex string '${hexString}' to convert to bytes!`)
		arr[i] = v1 * 16 + v2
	}
	return arr
}

function codeToNumber(c: number) {
	return c > 96 ? c - 96 + 9 : c > 64 ? c - 64 + 9 : c > 57 ? -1 : c - 48
}

export function encode(arr: Uint8Array) {
	if (!arr)
		return ''
	let str = ''
	for (const v of arr) {
		const s = v.toString(16)
		str += s.length < 2 ? '0' + s : s
	}
	return str
}
