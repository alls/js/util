/**
 * Parse number string. `parseInt` with default value and without NaN.
 * @param str String to be parsed as number.
 * @param defaultValue Default value if string is not a number.
 * @returns Parsed number or default value.
 */
function parseInteger(str: string | null | undefined): number | undefined
function parseInteger(str: string | null | undefined,
	defaultValue: number): number
function parseInteger(str: string | null | undefined, defaultValue?: number) {
	if (!str) return defaultValue
	const v: number | undefined = parseInt(str)
	return v !== undefined && !isNaN(v) ? v : defaultValue
}
export { parseInteger as parseInt }

/**
 * Parse number string. `parseFloat` with default value and without NaN.
 * @param str String to be parsed as number.
 * @param defaultValue Default value if string is not a number.
 * @returns Parsed number or default value.
 */
function parseNumber(str: string | null | undefined): number | undefined
function parseNumber(str: string | null | undefined,
	defaultValue: number): number
function parseNumber(str: string | null | undefined, defaultValue?: number) {
	if (!str) return defaultValue
	const v: number | undefined = parseFloat(str)
	return v !== undefined && !isNaN(v) ? v : defaultValue
}
export { parseNumber }

export function parseNumbers(str: string) {
	return str ? str.split(',').map(v => parseInt(v))
		.filter(v => v !== void 0 && !isNaN(v)) : []
}

/**
 * JSON.stringify replacer to format bigint numbers as '123n' strings.
 * @param _key JSON key. Unused.
 * @param value JSON value. Will be replaced if bigint.
 * @returns Replaced or given value.
 */
export function bigIntReplacer(_key: string, value: any) {
	return typeof value === 'bigint' ? value.toString() + 'n' : value as unknown
}

/**
 * JSON.parse reviver to read '123n' strings as bigint numbers.
 * @param _key JSON key. Unused.
 * @param value JSON value. '123n' string get replaced with bigint number.
 * @returns Replaced or given value.
 */
export function bigIntReviver(_key: string, value: any) {
	return typeof value === 'string' && /^[+-]?\d+n$/.test(value)
		? BigInt(value.slice(0, -1))
		: value as unknown
}
