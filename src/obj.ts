import * as any from './any.js'
import * as array from './array.js'

/**
 * Filter object properties.
 * @param obj Object to filter properties from.
 * @param predicate Function to test a property with.
 * @returns New object with only the filtered properties.
 */
export function filterProps<T extends object>(obj: T,
	predicate: (val: T[keyof T], _key: keyof T) => boolean) {
	if (!obj || typeof obj !== 'object') return obj
	const res: Partial<T> = {}
	for (const k of Object.getOwnPropertyNames(obj) as (keyof T)[]) {
		const v = obj[k]
		if (predicate(v, k))
			res[k] = v
	}
	return res
}

/**
 * Map object property keys.
 * @param obj Object to map property keys from.
 * @param transform Function to transform each property key.
 * @returns New object with the property values under a new key.
 */
export function mapKeys<T extends object, V extends string | number | symbol>(
	obj: T, transform: (key: keyof T) => V) {
	if (!obj || typeof obj !== 'object')
		throw new Error(
			`Cannot map keys of ${obj === null ? 'null' : typeof obj}!`)
	const res = {} as unknown as { [k in V]: T[keyof T] }
	for (const k of Object.getOwnPropertyNames(obj) as (keyof T)[]) {
		const v = transform(k)
		res[v] = obj[k]
	}
	return res
}

/**
 * Map object property values.
 * @param obj Object to map property values from.
 * @param transform Function to transform each property value.
 * @returns New object to own the properties found with their transformed value.
 */
export function mapValues<T extends object, V>(obj: T,
	transform: (val: T[keyof T], _key: keyof T) => V) {
	if (!obj || typeof obj !== 'object')
		throw new Error(
			`Cannot map values of ${obj === null ? 'null' : typeof obj}!`)
	const res = {} as unknown as { [k in keyof T]: V }
	for (const k of Object.getOwnPropertyNames(obj) as (keyof T)[]) {
		const v = transform(obj[k], k)
		res[k] = v
	}
	return res
}

/** Type agnostic Object.keys() */
export const keys = Object.keys as <T extends object>(obj: T) => (keyof T)[]

/**
 * Compares to objects having the same key set.
 * @param a First object to compare.
 * @param b Second object to compare.
 * @returns True, if both object have an equal key set. False otherwise.
 */
export function equalKeys<A extends object, B extends object>(a: A, b: B) {
	if (!a)
		return !b
	if (typeof a !== 'object')
		return a == b
	const keysA = Object.keys(a)
	for (const k of keysA) {
		if (!(k in b))
			return false
	}
	return Object.keys(b).length === keysA.length
}

/**
 * Deletes properties within the given object.
 * A missing key is ignored.
 * @param obj Object to delete properties from.
 * @param keys Keys of the properties to delete.
 * @returns The given object without the corresponding properties.
 */
export function deletePropsIn<T extends object, K extends keyof T>(
	obj: T, ...keys: K[]) {
	for (const k of keys)
		delete obj[k]
	return obj as Omit<T, K>
}

/**
 * Deletes properties within the given object with equal values in the
 * comparer object.
 * A missing keys in the comparer object are ignored.
 * @param obj Object to delete properties from.
 * @param comparer Object with similar properties.
 * @returns The given object without the corresponding properties.
 */
export function deleteEqualPropsIn(obj: object, comparer: object) {
	for (const k of Object.keys(obj))
		if (k in comparer &&
			obj[k as keyof typeof obj] === comparer[k as keyof typeof obj])
			delete obj[k as keyof typeof obj]
	return obj
}

/** Compare two values (objects, arrays, primitives) to be deeply equal.
 * Ignore undefined object properties or array elements.
 * Circular references are not allowed!
 */
export function deepEquals<T = unknown>(a: T, b: T,
	excludes?: (string | number)[] | { [k: string | number]: unknown }) {
	if (a === b)
		return true
	if (any.isPrimitive(a))
		return a === b
	if (any.isPrimitive(b))
		return false
	if (excludes && Array.isArray(excludes))
		excludes = array.toObject(excludes)
	const keysA = Object.keys(a as object)
		.filter(k => !(excludes && k in excludes))
		.filter(k => (a as Record<string, unknown>)[k] !== void 0)
	const keysB = Object.keys(b as object)
		.filter(k => !(excludes && k in excludes))
		.filter(k => (b as Record<string, unknown>)[k] !== void 0)
	if (keysA.length !== keysB.length)
		return false
	for (const k of keysA) {
		if (!(k in (b as object)))
			return false
		if (!deepEquals((a as Record<string, unknown>)[k],
			(b as Record<string, unknown>)[k],
			excludes))
			return false
	}
	return true
}

/**
 * Test the given object to have no members.
 * @param obj Object to test.
 * @returns No members in the object ({}).
 */
export function isEmpty(obj: any): boolean {
	for (const _k in obj)
		return false
	return true
}

/** Swap object property keys with their values. Result for duplicate values
 * is not defined. Requires to provide values valid as keys.
 */
export function swapKeysWithValuesIn(obj: Record<string, string>) {
	const res: Record<string, string> = {}
	for (const k of keys(obj)) res[obj[k]] = k
	return res
}

/**
 * Copies only certain properties into a new object.
 * A missing key is ignored.
 * @param src Object to copy properties from.
 * @param keys Keys of the properties to include.
 * @returns A new object with only the given properties.
 */
export function include<T extends object, K extends string | number | symbol>(
	src: T, ...keys: K[]) {
	const res = {} as { [k in (K & keyof T)]: T[k] }
	for (const k of Object.keys(src)) {
		if (keys.includes(k as K))
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			res[k as (K & keyof T)] = src[k as keyof T] as any
	}
	return res
}

/**
 * Copies the properties of a given object, without the excluded ones,
 * into a new object.
 * A missing key is ignored.
 * @param src Object to copy properties from.
 * @param keys Keys of the properties to exclude.
 * @returns A new object with the properties of the given object except the 
 * excluded ones.
 */
export function exclude<T extends object, K extends string | number | symbol>(
	src: T, ...keys: K[]) {
	const res = {} as Omit<T, K>
	for (const k of Object.keys(src)) {
		if (!keys.includes(k as K))
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			res[k as keyof Omit<T, K>] = src[k as keyof T] as any
	}
	return res
}

/**
 * Excludes properties from a copy of the given object with equal values in the
 * comparer object.
 * A missing keys in the comparer object are ignored.
 * @param obj Object to copy properties from.
 * @param comparer Object with similar properties.
 * @returns A copy of the given object without the corresponding properties.
 */
export function excludeEqualProps<T extends object, C extends object>(obj: T,
	comparer: C) {
	const res = {} as T
	for (const k of Object.keys(obj))
		if (!(k in comparer) ||
			obj[k as keyof T] !== comparer[k as keyof C] as unknown)
			res[k as keyof T] = obj[k as keyof T]
	return res
}

/**
 * Copies an object with only the non-null and non-undefined properties.
 * @param obj Source object to copy properties from.
 * @returns Object with only the properties of the given object with 
 * 					values not null or undefined.
 */
export function excludeNullProps<T extends object>(obj: T) {
	return Object.entries(obj).reduce((res, [key, value]) => {
		if (value !== null && value !== undefined)
			res[key as keyof T] = value as T[keyof T]
		return res
	}, {} as T) as { [K in keyof T]: Exclude<T[K], null | undefined> }
}

/**
 * Copies an object with only the non-empty, non-null and non-undefined
 * properties.
 * @param obj Source object to copy properties from.
 * @returns Object with only the properties of the given object with 
 * 					values not empty, null or undefined.
 */
export function excludeEmptyProps<T extends object>(obj: T) {
	return Object.entries(obj).reduce((res, [key, value]) => {
		if (value !== null && value !== undefined && value !== '')
			res[key as keyof T] = value as T[keyof T]
		return res
	}, {} as T) as { [K in keyof T]: Exclude<T[K], null | undefined> }
}

/**
 * Find a property value in the given object.
 * @param obj Object to find a property in.
 * @param predicate Predicate to test a property with.
 * @returns The first property value found to match the predicate.
 */
export function findProp<T extends object>(obj: T,
	predicate: (v: T[keyof T], k?: keyof T) => boolean) {
	for (const k of Object.keys(obj)) {
		const v = obj[k as keyof T]
		if (predicate(v, k as keyof T))
			return v
	}
	return null
}

/**
 * Test the given object to contain all property values of the reference.
 * @param obj Object to test properties of.
 * @param some Reference object to compare properties with.
 * @returns True, if the given object contains all property values of the
 * reference.
 */
export function contains<T extends object>(obj: T, some: Partial<T>) {
	for (const k of keys(some))
		if (obj[k] !== some[k])
			return false
	return true
}
