import createBaseX from './3rdParty/base-x.js'
import { replaceCharAt } from './str.js'

// cspell:disable
export const alphabet = {
	base58: '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
	base62: '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
	// similar to Crockford. optimized for lower case. u instead of s.
	// https://github.com/agnoster/base32-js
	base32: '0123456789abcdefghjkmnpqrtuvwxyz',
	// https://www.crockford.com/base32.html
	crockford: '0123456789ABCDEFGHJKMNPQRSTVWXYZ',
}
// cspell:enable

export const base58 = createCoder(alphabet.base58)
export const base62 = createCoder(alphabet.base62)
export const base32 = createCoder(alphabet.base32)

export function createCoder(alphabet: string) {
	const base = alphabet.length
	const baseX = createBaseX(alphabet)
	const decode = alphabet.split('')
		.reduce((m, c, i) => ({ ...m, [c]: i }), {} as Record<string, number>)
	const bits = Math.ceil(Math.log2(base))
	return {

		encode(data: number | ArrayLike<number> | ArrayBufferLike) {
			let str = ''
			if (typeof data === 'number') {
				if (!Number.isInteger(data))
					throw new Error(`${data} is not an integer for base${base} encoding!`)
				if (data == 0)
					return alphabet[0]
				while (data > 0) {
					str = alphabet[data % base] + str
					data = Math.floor(data / base)
				}
			} else {
				const arr = data instanceof Uint8Array ? data : new Uint8Array(data)
				str = baseX.encode(arr)
					.padStart(Math.ceil(arr.length * 8 / bits), alphabet[0])
			}
			return str
		},

		decodeNumber(str: string) {
			let num = 0
			for (const c of str) {
				num = num * base
				num += decode[c]
			}
			return num
		},

		decodeBigInt(str: string) {
			const bigBase = BigInt(base)
			let v = 0n
			for (const c of str)
				v = v * bigBase + BigInt(decode[c])
			return v
		},

		decode(str: string) {
			const arr = baseX.decode(str)
			const len = Math.floor(str.length * bits / 8)
			if (arr.length > len)
				return arr.slice(arr.length - len)
			return arr
		},

		increment(str: string) {
			let s = str
			const maxIdx = base - 1
			for (let i = s.length - 1; i >= 0; --i) {
				const c = s[i]
				if (!(c in decode))
					throw new Error(`Invalid base${base} string '${str}'!`)
				const idx = decode[c]
				if (idx === maxIdx)
					s = replaceCharAt(s, i, alphabet[0])
				else
					return replaceCharAt(s, i, alphabet[idx + 1])
			}
			throw new Error(`Base${base} string '${str}' cannot be incremented!`)
		}

	}
}

export function bytesToBigInt(arr: Uint8Array) {
	let v = 0n
	for (const b of arr)
		v = (v << 8n) + BigInt(b)
	return v
}

export function bigIntToBytes(v: bigint, arr: Uint8Array) {
	for (let i = arr.length - 1; i >= 0; --i) {
		arr[i] = Number(v % 256n)
		v = v / 256n
	}
	return arr
}
