import * as blob from './blob.js'
import * as obj from './obj.js'

export const maxErrorMessageLength = 1000

function formatErrorMessage(msg: string) {
	return msg.length > maxErrorMessageLength ?
		msg.substring(0, maxErrorMessageLength) + '...' : msg
}

export interface ErrorData {
	name?: string
	message?: string
	stack?: string[]
	error?: ErrorData
}

export function toJson(err: unknown, inclStack = true, levels = 3)
	: ErrorData | null {
	return !err
		? null
		: typeof err === 'object'
			? levels > 0
				? {
					...obj.mapValues(err,
						(v: unknown) => blob.isBuffer(v)
							? formatErrorMessage(v.toString())
							: typeof v === 'string'
								? formatErrorMessage(v)
								: typeof v === 'object'
									? toJson(v, inclStack && v instanceof Error, levels - 1)
									: typeof v === 'bigint'
										? `${v}n`
										: v === null
											? void 0
											: v),
					stack: inclStack && (err as Error).stack?.split
						? (err as Error).stack?.split('\n')
							.map(v => formatErrorMessage(v.trim()))
						: void 0
				}
				: { message: 'sub levels skipped...' }
			: { message: formatErrorMessage(String(err)) }
}

