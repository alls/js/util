
export function isBlob(v: any): v is Blob {
	return Object.prototype.toString.call(v) === '[object Blob]'
}

export function isBuffer(v: any): v is Buffer {
	return typeof Buffer === 'undefined' ? false : v instanceof Buffer
}
