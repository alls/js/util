// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-parameters
export function tryParseJsonObject<T = unknown>(str: string) {
	if (!str)
		return null
	str = str.trim()
	if (str.startsWith('{') && str.endsWith('}')) {
		try {
			return JSON.parse(str) as T
		}
		catch {
			return null
		}
	}
	return null
}
