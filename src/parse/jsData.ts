export function parseJsData(txt: string) {
	const tmp: Record<string, unknown> = {}
	// eslint-disable-next-line @stylistic/max-len
	// eslint-disable-next-line @typescript-eslint/no-implied-eval, @typescript-eslint/no-unsafe-call
	Function('"use strict";return (function(tmp) { tmp.\n' + txt + ' })')()(tmp)
	const keys = Object.keys(tmp)
	if (keys.length <= 0)
		throw new Error(`Invalid JS data! '${txt}'`)
	return tmp[keys[0]]
}
