import * as crypto from 'crypto'

export function getRandomValues(length: number) {
	const buf = new Uint8Array(length)
	if (typeof self !== 'undefined' && self.crypto)
		self.crypto.getRandomValues(buf)
	else
		crypto.webcrypto.getRandomValues(buf)
	return buf
}
