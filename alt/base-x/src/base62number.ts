import baseXBuilder from 'base-x';
import * as base62 from '../../../src/base62.js';

interface Coder {
	encode: (num: number) => string
	decodeNumber: (str: string) => number
}

const baseX = baseXBuilder
	('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')

const runs = 100000;
let decoded: number

function numToBytes(num: number) {
	const arr = new Uint8Array(8);
	new DataView(arr.buffer, arr.byteOffset, arr.byteLength)
		.setBigUint64(0, BigInt(num));
	return arr;
}

function bytesToNum(bytes: Uint8Array) {
	return Number(new DataView(bytes.buffer, bytes.byteOffset, bytes.byteLength)
		.getBigInt64(0))
}

const baseX_coder = {
	encode: (num: number) => baseX.encode(numToBytes(num)),
	decodeNumber: (str: string) => bytesToNum(baseX.decode(str)),
}
// Built-in Base64 for reference
const base64 = {
	encode: (num: number) => Buffer.from(numToBytes(num)).toString('base64'),
	decodeNumber: (str: string) => bytesToNum(Buffer.from(str, 'base64')),
}

// different sizes
void [0, 1, 255, 256, 2 ** 32 - 1, Number.MAX_SAFE_INTEGER].forEach(num => {
	run(`base-x, ${num}`, num, baseX_coder);
	run(`base62, ${num}`, num, base62);
	run(`base64, ${num}`, num, base64);
})

function run(label: string, num: number, coder: Coder) {
	// console.log(Buffer.from(coder.encode(num), 'base64'))
	if (coder.decodeNumber(coder.encode(num)) !== num)
		throw new Error(`Invalid coding! ${label}: ${coder.encode(num)
			} ${coder.decodeNumber(coder.encode(num))}`)
	let code: string
	console.time(label + ', encode');
	for (let i = 0; i < runs; i++)
		code = coder.encode(num);
	console.timeEnd(label + ', encode');
	console.time(label + ', decode');
	for (let i = 0; i < runs; i++)
		decoded = coder.decodeNumber(code);
	console.timeEnd(label + ', decode');
}

/*

base-x, 0, encode: 54.387ms
base-x, 0, decode: 50.871ms
base62, 0, encode: 6.61ms
base62, 0, decode: 5.359ms
base64, 0, encode: 89.36ms
base64, 0, decode: 46.945ms
base-x, 1, encode: 54.71ms
base-x, 1, decode: 51.428ms
base62, 1, encode: 4.122ms
base62, 1, decode: 1.915ms
base64, 1, encode: 75.275ms
base64, 1, decode: 39.441ms
base-x, 255, encode: 48.719ms
base-x, 255, decode: 48.912ms
base62, 255, encode: 3.238ms
base62, 255, decode: 2.685ms
base64, 255, encode: 75.351ms
base64, 255, decode: 39.503ms
base-x, 256, encode: 48.802ms
base-x, 256, decode: 48.506ms
base62, 256, encode: 3.251ms
base62, 256, decode: 2.687ms
base64, 256, encode: 72.201ms
base64, 256, decode: 40.951ms
base-x, 4294967295, encode: 55.562ms
base-x, 4294967295, decode: 54.208ms
base62, 4294967295, encode: 12.062ms
base62, 4294967295, decode: 9.882ms
base64, 4294967295, encode: 68.164ms
base64, 4294967295, decode: 45.015ms
base-x, 9007199254740991, encode: 59.019ms
base-x, 9007199254740991, decode: 56.313ms
base62, 9007199254740991, encode: 17.652ms
base62, 9007199254740991, decode: 14.142ms
base64, 9007199254740991, encode: 68.818ms
base64, 9007199254740991, decode: 43.741ms

 */
