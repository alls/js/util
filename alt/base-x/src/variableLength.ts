import baseXBuilder from 'base-x';

const baseX = baseXBuilder
	('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')

// variable length
console.log(baseX.encode([0x00, 0x00, 0x00, 0x01])) // 0001
console.log(baseX.encode([0x00, 0x00, 0x01])) // 001
console.log(baseX.encode([0x00, 0xff, 0xff])) // 0H31
console.log(baseX.encode([0x01, 0xff, 0xff])) // Y63
console.log(baseX.encode([0xff, 0xff, 0xff])) // 18OWF

console.log(baseX.encode([0x00])) // 0
console.log(baseX.decode('0')) // [0]
console.log(baseX.encode([0x00, 0x00])) // 00
console.log(baseX.decode('00')) // [0, 0]
console.log(baseX.encode([0x00, 0x00, 0x00])) // 000
console.log(baseX.decode('000')) // [0, 0, 0]
