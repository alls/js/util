import baseXBuilder from 'base-x';
import * as base62 from '../../../src/base62.js';

interface Coder {
	encode: (buf: Uint8Array) => string
	decode: (str: string) => Uint8Array
}

const baseX = baseXBuilder
	('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')

const buf20 = new Uint8Array([0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33,
	0x14, 0x18, 0xb6, 0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4]);
const runs = 10000;
let decoded: Uint8Array

// Built-in Base64 for reference
const base64 = {
	encode: (buf: Uint8Array) => Buffer.from(buf).toString('base64'),
	decode: (str: string) => Buffer.from(str, 'base64') as Uint8Array,
}

// different sizes
void [20, 16, 8, 4].forEach(numBytes => {
	const buf = buf20.slice(0, numBytes);
	run(`base-x, ${numBytes} bytes`, buf, baseX);
	run(`base62, ${numBytes} bytes`, buf, base62);
	run(`base64, ${numBytes} bytes`, buf, base64);
})

function run(label: string, buf: Uint8Array, coder: Coder) {
	const codes: string[] = []
	console.time(label + ', encode');
	for (let i = 0; i < runs; i++)
		codes.push(coder.encode(buf));
	console.timeEnd(label + ', encode');
	console.time(label + ', decode');
	for (const code of codes)
		decoded = coder.decode(code);
	console.timeEnd(label + ', decode');
}

/*

base-x, 20 bytes, encode: 16.028ms
base-x, 20 bytes, decode: 22.167ms
base62, 20 bytes, encode: 62.012ms
base62, 20 bytes, decode: 67.79ms
base64, 20 bytes, encode: 9.292ms
base64, 20 bytes, decode: 7.831ms
base-x, 16 bytes, encode: 7.401ms
base-x, 16 bytes, decode: 7.467ms
base62, 16 bytes, encode: 47.408ms
base62, 16 bytes, decode: 49.39ms
base64, 16 bytes, encode: 3.458ms
base64, 16 bytes, decode: 5.023ms
base-x, 8 bytes, encode: 2.724ms
base-x, 8 bytes, decode: 3.265ms
base62, 8 bytes, encode: 23.756ms
base62, 8 bytes, decode: 22.519ms
base64, 8 bytes, encode: 3.696ms
base64, 8 bytes, decode: 3.525ms
base-x, 4 bytes, encode: 1.632ms
base-x, 4 bytes, decode: 2.013ms
base62, 4 bytes, encode: 10.703ms
base62, 4 bytes, decode: 11.514ms
base64, 4 bytes, encode: 3.468ms
base64, 4 bytes, decode: 2.412ms

 */
