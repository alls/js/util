import baseXBuilder from 'base-x';
import * as base32 from '../../../src/base32.js';

interface Coder {
	encode: (buf: Uint8Array) => string
	decode: (str: string) => Uint8Array
}

// cspell:disable
const baseX = baseXBuilder('0123456789abcdefghjkmnpqrtuvwxyz')
// cspell:enable

const buf20 = new Uint8Array([0x17, 0x05, 0x76, 0x84, 0xbe, 0xa1, 0xf9, 0x33,
	0x14, 0x18, 0xb6, 0x33, 0xa8, 0xf3, 0x73, 0x11, 0x9d, 0x76, 0x5f, 0xd4]);
const runs = 100000;
let decoded: Uint8Array

// different sizes
void [20, 16, 8, 4].forEach(numBytes => {
	const buf = buf20.slice(0, numBytes);
	run(`base-x, ${numBytes} bytes`, buf, baseX);
	run(`base32, ${numBytes} bytes`, buf, base32);
})

function run(label: string, buf: Uint8Array, coder: Coder) {
	const codes: string[] = []
	console.time(label + ', encode');
	for (let i = 0; i < runs; i++)
		codes.push(coder.encode(buf));
	console.timeEnd(label + ', encode');
	console.time(label + ', decode');
	for (const code of codes)
		decoded = coder.decode(code);
	console.timeEnd(label + ', decode');
}

/*

base-x, 20 bytes, encode: 185.252ms
base-x, 20 bytes, decode: 162.765ms
base32, 20 bytes, encode: 91.058ms
base32, 20 bytes, decode: 109.245ms
base-x, 16 bytes, encode: 92.152ms
base-x, 16 bytes, decode: 98.148ms
base32, 16 bytes, encode: 65.83ms
base32, 16 bytes, decode: 79.296ms
base-x, 8 bytes, encode: 37.586ms
base-x, 8 bytes, decode: 40.337ms
base32, 8 bytes, encode: 33.532ms
base32, 8 bytes, decode: 35.535ms
base-x, 4 bytes, encode: 19.121ms
base-x, 4 bytes, decode: 20.242ms
base32, 4 bytes, encode: 19.914ms
base32, 4 bytes, decode: 17.319ms

 */
