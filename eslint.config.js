import config from '@alls/eslint-config'
import ts from 'typescript-eslint'

export default ts.config(
	...config,
	{
		languageOptions: {
			parserOptions: {
				projectService: { allowDefaultProject: ['*.config.js', '*.config.ts'] },
				tsconfigRootDir: import.meta.dirname,
			},
		},
	},
)
